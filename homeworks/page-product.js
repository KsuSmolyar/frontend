'use strict';

const form = document.querySelector('.feedback-form');
const formInputs = form.querySelectorAll('input'); 

const inputUserName = form['username'];
const inputEstimate = form['estimate'];
const textArea = form['review'];

let cart = [];
const cartButton = document.getElementById('cartButton');
const productData = {
    name: 'Смартфон Apple iPhone 13',
    color: 'Синий',
    memorySelected: 128,
};


const headerCart = document.querySelector('.header__cart');

const EMPTY_INPUT_USER_NAME_ERROR = 'Вы забыли указать имя и фамилию';
const LENGTH_INPUT_USER_NAME_ERROR = 'Имя не может быть короче 2-х символов';
const INPUT_ESTIMATE_ERROR = 'Оценка должна быть от 1 до 5';
const getInputName = (elem) => elem.getAttribute('name');




function addErrorClass(input) {
    input.classList.add('error');
}

function removeErrorClass(input) {
    input.addEventListener('focus', () => {
        input.classList.remove('error');
    })
}

function saveToLocalStorage(input) {
    input.addEventListener('input', () => {
        localStorage.setItem(getInputName(input), input.value);
    })
}

function getFromLocalStorage(input) {
    input.value = localStorage.getItem(getInputName(input))
}

formInputs.forEach((input) => {
    removeErrorClass(input);
    saveToLocalStorage(input)
})

saveToLocalStorage(textArea);

document.addEventListener('DOMContentLoaded', () => {
    getFromLocalStorage(inputUserName);
    getFromLocalStorage(inputEstimate);
    getFromLocalStorage(textArea);
    restoreCart();
    changeCounter();
    changeButtonState();
})

form.onsubmit = function(event) {
    event.preventDefault();
    const inputUserNameValue = inputUserName.value.trim();
    const inputEstimateValue = inputEstimate.value.trim();
    
    let text = '';
    if (inputUserNameValue === '' || inputUserNameValue.length <= 2) {
        addErrorClass(inputUserName);
        text = inputUserNameValue === '' ? EMPTY_INPUT_USER_NAME_ERROR : LENGTH_INPUT_USER_NAME_ERROR;  
    }
    

    if (text === '' && (inputEstimateValue === '' || inputEstimateValue > 5 || inputEstimateValue < 1 || Number.isNaN(+inputEstimateValue))) {
        addErrorClass(inputEstimate);
        text = INPUT_ESTIMATE_ERROR;
    }

    if(text!== '') {
        form.querySelector('.findings__input.error ~ .findings__error').textContent = text;
    } else {
        localStorage.clear();
        inputUserName.value = '';
        inputEstimate.value = '';
        textArea.value = '';
    }
    
}

//Домашка 27

function addOrRemoveItemFromCart () {
    const storedCart = localStorage.getItem('cart');
    if (storedCart) {
        const item = JSON.parse(storedCart).find(item => item.name === productData.name)
        if (item) {
            cart = JSON.parse(storedCart).filter(function (item) {
                return item.name !== productData.name
            })
        } else {
            cart.push(productData);
        }
        localStorage.setItem('cart', JSON.stringify(cart));
    } else {
        cart.push(productData);
        localStorage.setItem('cart', JSON.stringify(cart));
    }
}

cartButton.addEventListener ('click', () => {
    addOrRemoveItemFromCart()
    changeCounter()
    changeButtonState()
    }
);

function restoreCart() {
    const storedCart = localStorage.getItem('cart');
    if(storedCart) {
        cart = JSON.parse(storedCart);
    }
}

function changeCounter() {
    headerCart.dataset.count = cart.length;
    if (cart.length > 0) {
        headerCart.classList.add('show-counter');
    } else {
        headerCart.classList.remove('show-counter');
    }
}

function changeButtonState() {
    const prod = cart.find(prod => prod.name === productData.name)
    
    if(prod) {
        cartButton.textContent = 'Товар уже в корзине';
        cartButton.classList.add('btn_third');
    } else {
        cartButton.textContent = 'Добавить в корзину';
        cartButton.classList.remove('btn_third');
    }
}
