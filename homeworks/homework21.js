'use strict'
//Упражение №1

let user = {};

/**
 * Проверяет является ли объект пустым
 * @param {Object} obj объект, который проверяем
 * @returns {boolean} возвращает true, если объект пустой 
 */
function isEmpty(obj) {
    
    if (Object.keys(obj).length > 0) {
        return false;
    }
    return true;
}

 console.log(isEmpty(user));



//Упражнение №2 В файле data.js

//Упражнение №3

const salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

/**
 * Увеличивает зарплату на переданный процент, округляя до целого в меньшую сторону
 * @param {number} perzent процент, на который увеличиваем зарплату
 * @returns {Object<string, number>} возвращает объект с измененными зарплатами
 */

function raiseSalary(perzent) {
    for(let key in salaries) {
        const salary = salaries[key];
        salaries[key] = Math.floor(salary + salary * (perzent / 100));
    }
    return salaries;
}

raiseSalary(7);

/**
 * Считает сумму зарплат
 * @param {Object<string, number>} obj объект с зарплатами
 * @returns {number} возвращает сумму зарплат
 */
function salarySumm(obj) {
    let summ = 0;

    for(let key in obj) {
        summ += obj[key];
    };

    return summ;
}

console.log(salarySumm(salaries));

