'use strict';

//Упражнение №1

/**
 * Ведет обратный отсчет от введенного пользователем числа
 * @returns {void}
 */
function countDown() {

    let message = prompt('Введите число');

    if (message === null) return;
    message = message.trim();

    if (message === '' || Number.isNaN(+message)) {
        console.log('Вы ввели не число!');
        return;
    };
   
    let count = Math.abs(message);

    let intervalId = setInterval(() => {
        
        if (count === 0) {
            console.log("Время вышло");
            clearInterval(intervalId);
            return;
        }
        console.log(`Осталось ${count}`);
        count = count - 1;

    }, 1000);
        
};
/*
countDown();
*/
//Реализация через промис
new Promise((resolve, reject) => {
    let message = prompt('Введите число');

    if (message === null) return;
    message = message.trim();

    if (message === '' || Number.isNaN(+message)) {
        reject('Вы ввели не число!');
        return;
    };
    
    let count = Math.abs(message);

    let intervalId = setInterval(() => {
        
        if (count === 0) {
            resolve("Время вышло");
            clearInterval(intervalId);
            return;
        }
        console.log(`Осталось ${count}`);
        count = count - 1;

    }, 1000);
        
})
.then(function(response) {
    console.log(response);
})
.catch(function(err) {
    console.log(err);
})


//Упражнение №2

fetch("https://reqres.in/api/users")
    .then(function(response) {
        return response.json();
    })
    .then(function(response) {
        let data = response.data;
        let message = `Получили пользователей: ${data.length}\n`

        data.forEach(element => {
            let userInfo = `- ${element.first_name} ${element.last_name} (${element.email})\n`;
            message += userInfo;
        });
        console.log(message);
    })
    .catch((err) => {
        console.log(`Ошибка сервера ${err}`);
        
    })


