'use strict'

class Form {
    constructor(formSelector) {
        this.form = document.querySelector(formSelector);
        this.formInputs = [
            ...this.form.querySelectorAll('input'),
            ...this.form.querySelectorAll('textarea'),
        ];
        this.formInputs.forEach(input => {
            this.getFromLocalStorage(input);
            this.saveToLocalStorage(input);
        }) 
    }

    getInputName = (elem) => elem.getAttribute('name');

    saveToLocalStorage(input) {
        input.addEventListener('input', () => {
            localStorage.setItem(this.getInputName.call(this, input), input.value);
        })
    }

    getFromLocalStorage(input) {
        input.value = localStorage.getItem(this.getInputName.call(this, input));
    }

    initOnSubmit() {
        this.form.addEventListener('submit', this.onSubmit.bind(this));
    }

    clearForm() {
        localStorage.clear();
        this.formInputs.forEach(input => input.value = '');
        alert (`Форма успешно отправлена!`);

    }

    onSubmit(event) {
        event.preventDefault();
        this.clearForm()
    }
}


class AddReviewForm extends Form {
    EMPTY_INPUT_USER_NAME_ERROR = 'Вы забыли указать имя и фамилию';
    LENGTH_INPUT_USER_NAME_ERROR = 'Имя не может быть короче 2-х символов';
    INPUT_ESTIMATE_ERROR = 'Оценка должна быть от 1 до 5';

    constructor(form) {
        super(form);
        this.formInputs.forEach(this.removeErrorClass)
    }

    addErrorClass(input) {
        input.classList.add('error');
    }

    removeErrorClass(input) {
        input.addEventListener('focus', () => {
            input.classList.remove('error');
        })
    }

    validateForm() {
        const inputUserName = this.form['username'];
        const inputEstimate = this.form['estimate'];
        const inputUserNameValue = inputUserName.value.trim();
        const inputEstimateValue = inputEstimate.value.trim();
        
        let text = '';
        if (inputUserNameValue === '' || inputUserNameValue.length <= 2) {
            this.addErrorClass(inputUserName);
            text = inputUserNameValue === '' ? this.EMPTY_INPUT_USER_NAME_ERROR : this.LENGTH_INPUT_USER_NAME_ERROR;  
        }
        

        if (text === '' && (inputEstimateValue === '' || inputEstimateValue > 5 || inputEstimateValue < 1 || Number.isNaN(+inputEstimateValue))) {
            this.addErrorClass(inputEstimate);
            text = this.INPUT_ESTIMATE_ERROR;
        }

        if(text!== '') {
            this.form.querySelector('.findings__input.error ~ .findings__error').textContent = text;
            return false;
        } else {
            return true;
        }
    }

    initOnSubmit() {
        this.form.addEventListener('submit', this.onSubmit.bind(this))
    }

    onSubmit(event) {
        event.preventDefault();
        const isValid = this.validateForm()

        if(isValid) {
            this.clearForm()
        }
    }
}

const reviewForm = new AddReviewForm('.feedback-form')
reviewForm.initOnSubmit()
