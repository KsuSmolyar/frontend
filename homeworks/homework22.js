'use strict';

//Упражнение №1

let arrOne = ["a", {}, 3, 3, -2];
/**
 * Считает сумму чисел в передаваемом массиве
 * @param {Array} arr массив, который передаем в функцию
 * @returns {Number} возвращает сумму чисел в массиве
 */

function getSumm(arr) {
    let summ = 0;

    arr.forEach((item) => {
        if (typeof item === 'number') summ += item;
    });

    return summ;
}

console.log(getSumm(arrOne));

//Упражнение №2 в файле data.js

//Упражнение №3

let cart = [4884,];

/**
 * Добавляет в корзину товар по индексу
 * @param {Number} id индекс товара
 */
function addToCart(id) {
    if(!cart.includes(id)) {
        cart.push(id);
    }
};

addToCart(3854);
console.log(cart);

addToCart(4884);
console.log(cart);

addToCart(6578);
console.log(cart);
/**
 * Удаляет из корзины товар по индексу
 * @param {Number} id индекс товара
 */

function removeFromCart(id) {
    let cartIndex = cart.indexOf(id);

    if(cartIndex !== -1) {
        cart.splice(cartIndex, 1);
    }
};

removeFromCart(3854);
console.log(cart); 



